;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages linux)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system trivial)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages linux)
  #:use-module (srfi srfi-1))

(define-public linux-nonfree
  (package
    (inherit linux-libre)
    (name "linux-nonfree")
    (version "5.1.1")
    ;; To follow linux-libre version, we can use the following, but this will
    ;; break if we don't immediately update the checksum.  (version
    ;; (package-version linux-libre))
    (source
     (origin
      (method url-fetch)
      (uri
       (string-append
        "https://cdn.kernel.org/pub/linux/kernel/v"
        (version-major version)
        ".x/linux-" version ".tar.xz"))
      (sha256
       (base32
        "1pcd0npnrjbc01rzmm58gh135w9nm5mf649asqlw50772qa9jkd0"))))
    (home-page "https://www.kernel.org/")
    (synopsis "Vanilla Linux kernel")
    (description "Vanilla Linux kernel which allows for non-free kernel modules / firmware.")
    ;; To build a custom kernel, pass it an alternate "kconfig":
    ;; (native-inputs
    ;;  `(("kconfig" ,(local-file "./linux-laptop.conf"))
    ;;    ,@(alist-delete "kconfig" (package-native-inputs linux-libre))))
    ))

;; > 250 MB.
(define-public linux-firmware-all
  (let ((version "0.0.0")
        (commit "6d5131107f2ba67a13f469ac770a55f101ba654d"))
    (package
     (name "linux-firmware-all")
     (version version)
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                    (commit commit)))
              (sha256
               (base32
                "0nql7rqkx064lsw5bh6n29yfdxmp3hl4nqgp1fxdb4ys76awchg3"))))
     (build-system trivial-build-system)
     (arguments
      `(#:modules ((guix build utils))
                #:builder (begin
                          (use-modules (guix build utils))
                          (let ((source (assoc-ref %build-inputs "source"))
                                (destination (string-append %output "/lib/firmware")))
                            (mkdir-p destination)
                            (copy-recursively source destination #:follow-symlinks? #t)
                            #t))))
     (home-page "")
     (synopsis "Non-free firmware for the Linux kernel")
     (description "This is a collection of non-free firmware for the Linux
kernel.  This can be useful to test if you need one of them to run a specific
piece of hardware.  This package is not recommended for every-day use: The
preferred way is to acquire free, open hardware of course.  Should this be ruled
out, Consider the more specific firmware packages that target only a single
piece of hardware (e.g. linux-firmware-amdgpu).")
     (license #f))))

(define %linux-firmware-commit "92e17d0dd2437140fab044ae62baf69b35d7d1fa")
(define %linux-firmware-version (git-version "20190502" "1" %linux-firmware-commit))

(define (linux-firmware-source)
  (origin
    (method git-fetch)
    (uri (git-reference
          (url (string-append "https://git.kernel.org/pub/scm/linux/kernel"
                              "/git/firmware/linux-firmware.git"))
          (commit %linux-firmware-commit)))
    (file-name (git-file-name "linux-firmware" %linux-firmware-version))
    (sha256
     (base32
      "1bsgp124jhs9bbjjq0fzmdsziwx1y5aivkgpj8v56ar0y2zmrw2d"))))

(define-public linux-firmware-amdgpu
  (package
    (name "linux-firmware-amdgpu")
    (version %linux-firmware-version)
    (source (linux-firmware-source))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let* ((source (assoc-ref %build-inputs "source"))
                         (fw-dir (string-append %output "/lib/firmware/"))
                         (bin-dir (string-append fw-dir "/amdgpu")))
                     (mkdir-p bin-dir)
                     (copy-recursively (string-append source "/amdgpu") bin-dir)
                     (install-file (string-append source "/LICENSE.amdgpu") fw-dir)
                     #t))))
    (home-page "http://support.amd.com/en-us/download/linux")
    (synopsis "Non-free firmware for AMD graphics chips")
    (description "While most AMD graphics cards can be run with the free Mesa,
many modern cards require a non-free kernel module to run properly, for example
support hibernation and advanced 3D.")
    (license (license:non-copyleft
              "https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/LICENSE.amdgpu"))))

;; ~60 MB.
(define-public linux-firmware-iwlwifi
  (package
    (name "linux-firmware-iwlwifi")
    (version %linux-firmware-version)
    (source (linux-firmware-source))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils))
                   (let ((source (assoc-ref %build-inputs "source"))
                         (fw-dir (string-append %output "/lib/firmware/")))
                     (mkdir-p fw-dir)
                     (for-each (lambda (file)
                                 (copy-file file
                                            (string-append fw-dir (basename file))))
                               (find-files source
                                           "iwlwifi-.*\\.ucode$|LICENSE\\.iwlwifi_firmware$"))
                     #t))))
    (home-page "https://wireless.wiki.kernel.org/en/users/drivers/iwlwifi")
    (synopsis "Non-free firmware for Intel wifi chips")
    (description "The proprietary iwlwifi kernel module is required by many
modern Intel wifi cards (commonly found in laptops).  This blob enables support
for 5GHz and 802.11ac, among others.")
    (license (license:non-copyleft
              "https://git.kernel.org/cgit/linux/kernel/git/firmware/linux-firmware.git/tree/LICENCE.iwlwifi_firmware?id=HEAD"))))
