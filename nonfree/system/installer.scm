;; Generate a bootable image (e.g. for USB sticks, etc.) with:
;; $ guix system disk-image applications/nonfree/system/installer.scm

(define-module (nonfree system installer)
  #:use-module (gnu system)
  #:use-module (gnu system install)
  #:use-module (nonfree packages linux)
  #:export (installation-os-nonfree))


(define installation-os-nonfree
  (operating-system
    (inherit installation-os)
    (kernel linux-nonfree)
    (firmware (append (list linux-firmware-iwlwifi)
                      %base-firmware))))

installation-os-nonfree
